const { invoke } = window.__TAURI__.tauri;
function onButtonHoverSelection(button_r, button_l, button_d, button_u, button) {
  if(button.name !== "0"){
    if(button_r){
      if(button_r.name === "0"){button_r.className = "highlighted_empty"}
    }
    if(button_l){
      if(button_l.name === "0"){button_l.className = "highlighted_empty"}
    }
    if(button_d){
      if(button_d.name === "0"){button_d.className = "highlighted_empty"}
    }
    if(button_u){
      if(button_u.name === "0"){button_u.className = "highlighted_empty"}
    }
  }
}
function onButtonOutSelection(button_r, button_l, button_d, button_u){
  const button_types = ["button_empty", "button_attacker", "button_defender", "button_king"];
  if(button_r){button_r.className = button_types[parseInt(button_r.name)]}
  if(button_l){button_l.className = button_types[parseInt(button_l.name)]}
  if(button_d){button_d.className = button_types[parseInt(button_d.name)]}
  if(button_u){button_u.className = button_types[parseInt(button_u.name)]}
}
const parent_grid = document.getElementById("play-grid");

async function get_board_size(){
  return await invoke("get_board_size")
}
async function ai_setup(){
    return await invoke("ai_setup")
}

let size  = await get_board_size()
for (let i = 0; i < size; i++) {
  let div_row = document.createElement('div');
  div_row.className = "div_row";
  for (let j = 0; j < size; j++) {
    let play_button = document.createElement('button');
    const button_types = ["button_empty", "button_attacker", "button_defender", "button_king"];
    const name_lst = ['0','1','2','3'];
    invoke("get_space",{x:i,y:j}).then((type_num) => {
      play_button.className = button_types[type_num]
      play_button.name = name_lst[type_num]
      play_button.id = "play_button";
    })
    play_button.type = 'button';
    div_row.appendChild(play_button);
  }
  parent_grid.appendChild(div_row)
}
let ai_setup_status = await ai_setup()

const div_list = document.getElementsByClassName("div_row")
for (let i = 0; i < size; i++) {
  const button_list = div_list[i].children;
  for (let j = 0; j < size; j++) {
    button_list[j].addEventListener("click", function (){
          invoke("register_play_click",{x:i, y:j, name:this.name}).then((win_c) => {
            if(win_c !== 0){
              if(win_c === 1){
                document.getElementsByClassName("winMessage")[0].innerHTML = "The Attackers won"
              }
              if (win_c === 2) {
                document.getElementsByClassName("winMessage")[0].innerHTML = "The Defenders won"
              }
            }
          })
          for (let k = 0; k < size; k++) {
            let button_row = div_list[k].children;
            for (let l = 0; l < size; l++) {
              let play_button = button_row[l]
              const button_types = ["button_empty", "button_attacker", "button_defender", "button_king"];
              const name_lst = ['0','1','2','3'];
              invoke("get_space",{x:k,y:l}).then((type_num) => {
                play_button.className = button_types[type_num]
                play_button.name = name_lst[type_num]
                play_button.id = "play_button";
              })
            }
          }
        }
    )

    button_list[j].addEventListener("mouseover", function () {
      let button_d, button_u;
      if (!div_list[i + 1]) {
        button_d = null
      } else {
        button_d = div_list[i + 1].children[j]
      }
      if (!div_list[i - 1]) {
        button_u = null
      } else {
        button_u = div_list[i - 1].children[j]
      }
      onButtonHoverSelection(button_list[j+1], button_list[j-1], button_d, button_u, button_list[j])
    })
    button_list[j].addEventListener("mouseleave", function () {
      let button_d, button_u;
      if (!div_list[i + 1]) {
        button_d = null
      } else {
        button_d = div_list[i + 1].children[j]
      }
      if (!div_list[i - 1]) {
        button_u = null
      } else {
        button_u = div_list[i - 1].children[j]
      }
      onButtonOutSelection(button_list[j+1], button_list[j-1], button_d, button_u)
    })
  }
}
