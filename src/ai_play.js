const { invoke } = window.__TAURI__.tauri;

async function get_board_size(){
    return await invoke("get_board_size")
}
async function ai_setup(){
    return await invoke("ai_setup")
}
async function ai_play(player_num){
    return await invoke("ai_integration",{wt:player_num})
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function create_board(size) {
    const parent_grid = document.getElementById("play-grid");
    if (parent_grid){
        console.log(parent_grid)
    }
    for (let i = 0; i < size; i++) {
        let div_row = document.createElement('div');
        div_row.className = "div_row";
        for (let j = 0; j < size; j++) {
            let play_button = document.createElement('button');
            const button_types = ["button_empty", "button_attacker", "button_defender", "button_king"];
            const name_lst = ['0','1','2','3'];
            invoke("get_space",{x:i,y:j}).then((type_num) => {
                play_button.className = button_types[type_num]
                play_button.name = name_lst[type_num]
                play_button.id = "play_button";
            })
            play_button.type = 'button';
            div_row.appendChild(play_button);
        }
        parent_grid.appendChild(div_row)

    }
}
function clean_board() {
    const myNode = document.getElementById("play-grid");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.lastChild);
    }
}
let size = await get_board_size();
const next_button = document.getElementById("next_button");
let player_num = 1;
next_button.addEventListener("click", async function(){
    clean_board()
    create_board(size)
    if (player_num === 1) {
        player_num = 2
    }
    else {
        player_num = 1
    }
    await ai_play(player_num)
})

// while(true){
//     main()
//     console.log("loop")
//     sleep(100)
// }



