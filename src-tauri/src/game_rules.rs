use std::sync::Mutex;

pub(crate) struct GameB {
    pub(crate) board_size: Mutex<i8>,
    pub(crate) win_spaces: Mutex<Vec<(i8,i8)>>,
    pub(crate) board: Mutex<Vec<Vec<i8>>>,
    pub(crate) clean_board: Mutex<Vec<Vec<i8>>>
}

impl Default for GameB {
    fn default() -> Self {
        GameB {
            board_size: Mutex::new(0),
            win_spaces: Mutex::new(vec![]),
            board: Mutex::new(vec![]),
            clean_board: Mutex::new(vec![]),
        }
    }
}

pub(crate) fn create_clean_hen_board() -> GameB {
    const SIZE: usize = 11;
    let mut board: Vec<Vec<i8>> = vec![];
    for key in 0..SIZE {
        board.push(vec![]);
        for _ in 0..SIZE {
            board[key].push(0)
        }
    }
    // Attacker placements
    for x in 3..8{
        board[0][x] = 1;
        board[10][x] = 1;
        board[x][0] = 1;
        board[x][10] = 1;
    }
    board[1][5] = 1;
    board[5][1] = 1;
    board[9][5] = 1;
    board[5][9] = 1;

    // Defender Placements
    for i in 4..7{
        for y in 4..7{
            board[i][y] = 2;
            board[y][i] = 2;
        }
    }
    board[3][5] = 2;
    board[5][3] = 2;
    board[7][5] = 2;
    board[5][7] = 2;
    //King
    board[5][5] = 3;
    let mx: i8 = (SIZE - 1) as i8;
    GameB{
        board_size: Mutex::from(SIZE as i8),
        win_spaces: Mutex::from(vec![(0,mx),(mx,0),(mx,mx),(0,0)]),
        board: Mutex::from(board.clone()),
        clean_board: Mutex::from(board),
    }
}

pub(crate) fn create_clean_ard_board() -> GameB {
    const SIZE: usize = 7;
    let mut board: Vec<Vec<i8>> = vec![];
    for key in 0..SIZE {
        board.push(vec![]);
        for _ in 0..SIZE {
            board[key].push(0)
        }
    }
    // Attacker placements
    for x in 2..5{
        board[0][x] = 1;
        board[6][x] = 1;
        board[x][0] = 1;
        board[x][6] = 1;
    }
    board[5][3] = 1;
    board[3][5] = 1;
    board[3][1] = 1;
    board[1][3] = 1;
    // Defender Placements
    for i in 2..5{
        for y in 2..5{
            board[i][y] = 2;
            board[y][i] = 2;
        }
    }
    // King
    board[3][3] = 3;

    // generate win spaces
    let mut win_states: Vec<(i8,i8)> = vec![];
    let mx = (SIZE - 1) as i8;
    for x in 0..SIZE{
        win_states.push((x as i8, 0));
        win_states.push((x as i8, mx));
        win_states.push((0, x as i8));
        win_states.push((mx, x as i8));
    }
    GameB{
        board_size: Mutex::from(SIZE as i8),
        win_spaces: Mutex::from(win_states),
        board: Mutex::from(board.clone()),
        clean_board: Mutex::from(board),
    }
}

pub(crate) fn create_clean_bra_board() -> GameB {
    const SIZE: usize = 7;
    let mut board: Vec<Vec<i8>> = vec![];
    for key in 0..SIZE {
        board.push(vec![]);
        for _ in 0..SIZE {
            board[key].push(0)
        }
    }
    // Attacker placements
    for x in 0..2{
        board[3][x] = 1;
        board[x][3] = 1;

    }
    board[6][3] = 1;
    board[3][6] = 1;
    board[5][3] = 1;
    board[3][5] = 1;

    // Defender Placements
    board[3][4] = 2;
    board[4][3] = 2;
    board[2][3] = 2;
    board[3][2] = 2;
    // King
    board[3][3] = 3;
    let mx: i8 = (SIZE - 1) as i8;
    GameB{
        board_size: Mutex::from(SIZE as i8),
        win_spaces: Mutex::from(vec![(0,mx),(mx,0),(mx,mx),(0,0)]),
        board: Mutex::from(board.clone()),
        clean_board: Mutex::from(board),
    }
}