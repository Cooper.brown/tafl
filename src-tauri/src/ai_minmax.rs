// An implementation of the min max algorithm for the tauri games.
// -Cooper B


pub(crate) mod min_max {
    use std::cmp;
    use std::sync::MutexGuard;
    use tauri::State;
    use crate::ai::{GameState};
    use crate::game_rules::GameB;

    fn generate_children(state: &GameState, player: i8) -> Vec<GameState> {
        let mut board: Vec<Vec<i8>>;
        if state.board[0][0] != -1 {
            board = extend_board(&state.board);
        }
        else { board = state.board.clone(); }
        let mut children: Vec<GameState> = vec![];
        for x in 1..board.len() - 2 {
            for y in 1..board.len() - 2 {

                if ((board[x][y] != 0) & (board[x][y] == player)) | ((board[x][y] == 3) & (player == 2)) {
                    if board[x + 1][y] == 0 {
                        let mut b_left: i8 = board[x + 1][y];
                        if b_left == 0 {
                            let mut c_left: Vec<Vec<i8>> = board.clone();
                            c_left[x + 1][y] = player;
                            c_left[x][y] = 0;
                            children.push(GameState::min_max_new_child(c_left, x as i8, y as i8, 1))
                        }
                    }
                    if board[x - 1][y] == 0 {
                        let mut b_right: i8 = board[x - 1][y];
                        if b_right == 0 {
                            let mut c_right: Vec<Vec<i8>> = board.clone();
                            c_right[x - 1][y] = player;
                            c_right[x][y] = 0;
                            children.push(GameState::min_max_new_child(c_right,x as i8, y as i8, 2))
                        }
                    }
                    if board[x][y - 1] == 0 {
                        let mut b_up: i8 = board[x][y - 1];
                        if b_up == 0 {
                            let mut c_up: Vec<Vec<i8>> = board.clone();
                            c_up[x][y - 1] = player;
                            c_up[x][y] = 0;
                            children.push(GameState::min_max_new_child(c_up, x as i8, y as i8, 3))
                        }
                    }
                    if board[x][y + 1] == 0 {
                        let mut b_down: i8 = board[x][y + 1];
                        if b_down == 0 {
                            let mut c_down: Vec<Vec<i8>> = board.clone();
                            c_down[x][y + 1] = player;
                            c_down[x][y] = 0;
                            children.push(GameState::min_max_new_child(c_down, x as i8, y as i8, 4))
                        }
                    }
                }
            }
        }
        return children
    }

    fn terminal(state: &GameState, game_rules: &State<GameB>) -> i32 {
        // Returns 1 if attackers have the terminal state
        // 2 if the defenders are terminal, zero if neither
        let board_size: usize = state.board.len();
        let win_spaces: MutexGuard<Vec<(i8,i8)>> = game_rules.win_spaces.lock().unwrap();
        let mut king: (i8, i8) = (-1,-1);
        for x in 1..board_size -2{
            for y in 1..board_size -2{
                if state.board[x][y] == 3{
                    king = (x as i8, y as i8)
                }
            }
        }
        if king.0 == -1{
            return 1
        }
        for win_space in win_spaces.iter(){
            if (king.0 == win_space.0) & (king.1 == win_space.1){
                return 2
            }
        }
        return 0
    }
    fn pieces(state: &GameState) -> i32 {
        let board_size: usize = state.board.len() -2;
        let mut count: i32 = 0;
        for x in 0..board_size {
            for y in 0..board_size {
                if state.board[x][y] == 1{ count += 5 }
                else if state.board[x][y] ==2 { count -= 5 }
            }
        }
        return count
    }

    fn search_neighbors(state: &GameState, x: usize, y: usize) -> Vec<i8> {
        let mut neighbors: Vec<i8> = vec![];
        neighbors.push(state.board[x+1][y]);
        neighbors.push(state.board[x-1][y]);
        neighbors.push(state.board[x][y+1]);
        neighbors.push(state.board[x][y-1]);
        return neighbors;
    }

    fn cost(state: &GameState, game_rules: &State<GameB>) -> i32 {
        let terminal: i32 = terminal(state, game_rules);
        if terminal == 1 { return i32::MAX }
        else if terminal == 2 { return i32::MIN }

        let mut cost:i32 = pieces(state);
        let board_len: usize = state.board.len();
        let win_spaces: MutexGuard<Vec<(i8, i8)>> = game_rules.win_spaces.lock().unwrap();
        let mut distances: Vec<i32> = vec![];
        let mut king: (i8, i8) = (-1, -1);

        let mut king_neighbours_debug = 0;
        let mut defender_neighbours_debug = 0;


        for x in 1..board_len - 2 {
            for y in 1..board_len - 2 {
                if state.board[x][y] == 3 {
                    king = (x as i8, y as i8);
                    // King safety
                    let king_neighbors: Vec<i8> = search_neighbors(&state, x, y);
                    for neighbor in king_neighbors {
                        if neighbor == 1{
                            cost += 1;
                            king_neighbours_debug += 1
                        }
                    }
                }
                else if state.board[x][y] == 2 {
                    let defenders_neighbors: Vec<i8> = search_neighbors(&state, x, y);
                    for neigh in defenders_neighbors{
                        if neigh != 0 {
                            cost += 1;
                            defender_neighbours_debug+= 1
                        }
                    }
                }
            }
        }
        for x in 0..win_spaces.len() {
            let win_space: (i8, i8) = win_spaces[x];
            distances.push(((win_space.0 - king.0).abs() + (win_space.1 - king.1).abs()) as i32);

        }
        cost += *distances.iter().min().unwrap();
        println!("distances:  {:?}", distances);
        println!("king neighbors; {:?}", king_neighbours_debug);
        println!("defender neighbors: {:?}", defender_neighbours_debug);
        println!("cost:  {:?}", cost);
        return cost;
    }

    pub(crate) fn search(state: &GameState, game_rules: &State<GameB>, depth: i8, is_maximizing: bool, mut alpha: i32, mut beta: i32, mut prev_costs: Vec<i32>) -> (((i8, i8), i8), i32) {
        {
            if prev_costs.len() == 0{
                prev_costs = vec![]
            }
            if depth == 0 {
                if prev_costs.iter().min() == prev_costs.iter().max(){

                }
                return (state.next_move, cost(state, game_rules))
            }
            return if is_maximizing {
                let mut max_cost = i32::MIN;
                let mut max_state: GameState = GameState::default();
                for child in generate_children(state, 1) {
                    let cost: i32 = search(&child, game_rules, depth - 1, false, alpha, beta, (*prev_costs).to_owned()).1;
                    prev_costs.push(cost);
                    alpha = cmp::max(alpha, cost);
                    if max_cost <= cost {
                        max_cost = cost;
                        max_state = child;
                    }
                    if beta <= alpha {
                        break;
                    }
                }
                (max_state.next_move, max_cost)
            } else {
                let mut min_cost: i32 = i32::MAX;
                let mut min_state: GameState = GameState::default();
                for child in generate_children(state, 2) {
                    let cost: i32 = search(&child, game_rules, depth - 1, true, alpha, beta, (*prev_costs).to_owned()).1;
                    prev_costs.push(cost);
                    beta = cmp::min(beta, cost);
                    if min_cost >= cost {
                        min_cost = cost;
                        min_state = child;
                    }
                    if beta >= alpha {
                        break;
                    }
                }
                (min_state.next_move, min_cost)
            }
        }
    }

    // https://github.com/tensorflow/rust


    fn extend_board(board: &Vec<Vec<i8>>) -> Vec<Vec<i8>> {
        let mut new_board: Vec<Vec<i8>> = vec![];
        let mut blank_vec_1: Vec<i8> = vec![];
        for _ in 0..board.len() { blank_vec_1.push(-1) }
        new_board.push(blank_vec_1);
        for x in 0..board.len() - 0 {
            let mut x_vec: Vec<i8> = vec![];
            x_vec.push(-1);
            for y in 0..board.len() - 0 {
                x_vec.push(board[x][y])
            }
            x_vec.push(-1);
            new_board.push(x_vec)
        }
        let mut blank_vec_2: Vec<i8> = vec![];
        for _ in 0..board.len() { blank_vec_2.push(-1) }
        new_board.push(blank_vec_2);
        return new_board;
    }

    #[cfg(test)]
    mod tests {
        use crate::game_rules::create_clean_hen_board;
        use super::*;

        #[test]
        fn test_generate_children() {
            let board = create_clean_hen_board().board;
            let children_red = generate_children(
                &GameState {
                    board: board.lock().unwrap().clone(),
                    distance_to_win: 0,
                    cords_of_enemy: vec![],
                    next_move: ((0, 0), 0),
                }, 1);
            assert_eq!(children_red.len(), 36);
            let children_black = generate_children(
                &GameState {
                    board: board.lock().unwrap().clone(),
                    distance_to_win: 0,
                    cords_of_enemy: vec![],
                    next_move: ((0, 0), 0),
                }, 2);
            assert_eq!(children_black.len(), 20);
        }

        #[test]
        fn test_min_max() {
            let board = create_clean_hen_board().board;
        }
    }
}