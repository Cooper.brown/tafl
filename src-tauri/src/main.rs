// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command

mod ai;
mod game_rules;
mod ai_minmax;
mod ai_monte_carlo;

use game_rules::GameB;
use std::usize;
use std::sync::{Mutex, MutexGuard};
use std::borrow::Borrow;
use tauri::State;

struct ButtonObj {
    x: Mutex<i8>,
    y: Mutex<i8>,
    name: Mutex<i8>
}
struct LastUserClick {
    last_click_white: ButtonObj,
    last_click_black: ButtonObj,
}
struct TurnManager{
    white_turn: Mutex<bool>,
    ai: Mutex<i8>,
    ai_white: Mutex<bool>
}

#[tauri::command]
fn get_space(pa_state:State<'_,GameB>, x:usize, y:usize) -> i8{
    let mut play_area = &pa_state.board;
    let mut pa = play_area.lock().unwrap();
    return pa[x][y].into();
}

#[tauri::command]
fn get_board_size(pa_state:State<'_,GameB>)-> i8{
    return *pa_state.board_size.lock().unwrap();
}

#[tauri::command]
fn ai_setup() -> i8{
    // 1 = AI black, 2 = AI white, 3 = AI both
    return 3;
}

#[tauri::command]
fn register_play_click(pa_state:State<'_,GameB>, lc_state: State<'_, LastUserClick>, turn_table:State<'_, TurnManager>, x:i8, y:i8, name:String) -> i8{
    let mut tt = &turn_table.white_turn;
    let mut last_click;
    let mut turn_table_int: i8;
    let name: i8 = name.parse().unwrap();

    if *tt.lock().unwrap(){
        last_click = &lc_state.last_click_white;
        turn_table_int = 2;
    }
    else {
        last_click = &lc_state.last_click_black;
        turn_table_int = 1;
    }

    let last_click_int = *last_click.name.lock().unwrap();
    if (turn_table_int == last_click_int) | (last_click_int == 0) |  ((turn_table_int == 2) & (last_click_int ==3)) {
        let prev_usr_click: ButtonObj =  ButtonObj {
            x: Mutex::new(*last_click.x.lock().unwrap()),
            y: Mutex::new(*last_click.y.lock().unwrap()),
            name: Mutex::new(*last_click.name.lock().unwrap())
        };
        *last_click.x.lock().unwrap() = x;
        *last_click.y.lock().unwrap() = y;
        *last_click.name.lock().unwrap() = name;
        let mut play_area = &pa_state.board;
        let mut pa = play_area.lock().unwrap();
        if (*prev_usr_click.x.lock().unwrap() == x) & (*prev_usr_click.y.lock().unwrap() == y) {
            if *tt.lock().unwrap(){
                *tt.lock().unwrap() = false;
            }
            else {
                *tt.lock().unwrap() = true;
            }
            return 0
        };
        if (*prev_usr_click.name.lock().unwrap() != 0) & (name == 0) {
            if ((*prev_usr_click.y.lock().unwrap()-y).abs() == 1) ^ ((*prev_usr_click.x.lock().unwrap()-x).abs() == 1) {
                pa[x as usize][y as usize] = *prev_usr_click.name.lock().unwrap();
                pa[*prev_usr_click.x.lock().unwrap() as usize][*prev_usr_click.y.lock().unwrap() as usize] = 0;
                let a = !*<MutexGuard<'_, bool> as Into<MutexGuard<'_, bool>>>::into(tt.lock().unwrap());
                *tt.lock().unwrap() = a;
            }
        }
        let lost_piece = check_lost_piece(pa);
        let x = *lost_piece.x.lock().unwrap();
        if x != -1{
            let mut play_area = &pa_state.board;
            let mut pa = play_area.lock().unwrap();
            let x_ind = *lost_piece.x.lock().unwrap() as usize;
            let y_ind = *lost_piece.y.lock().unwrap() as usize;
            pa[x_ind][y_ind] =  0
        }
    } else { *last_click.name.lock().unwrap() = turn_table_int }
    let mut play_area = &pa_state.board;
    let pa = play_area.lock().unwrap();
    drop(pa);
    let win = check_win(pa_state);
    return win
}

#[tauri::command]
fn ai_integration(play_area:State<'_,GameB>, wt:i8) -> i8 {
    let mut white_turn: bool;
    if  wt == 1 {
        white_turn = true;
    }
    else {
        white_turn = false;
    }
    let search_results = ai::ai(
        play_area.board.lock().unwrap().clone(),
        &play_area,
        white_turn
    );
    let mut pa = play_area.board.lock().unwrap();
    let mut ai_cords: (i8, i8) = search_results.0.0;
    ai_cords.0 -= 1;
    ai_cords.1 -= 1;
    let ai_direction: i8  = search_results.0.1;
    println!("{:?}", ai_cords);
    if (ai_cords.0 == -1){
        return 2
    }
    println!("{:?}", ai_cords.1);
    println!("{:?}", ai_cords.0);

    let ai_name = pa[ai_cords.0 as usize][ai_cords.1 as usize];
    pa[ai_cords.0 as usize][ai_cords.1 as usize] = 0;
    if ai_direction == 1 { pa[ai_cords.0 as usize + 1][ai_cords.1 as usize] = ai_name; }
    else if ai_direction == 2 { pa[ai_cords.0 as usize - 1][ai_cords.1 as usize] = ai_name; }
    else if ai_direction == 3 { pa[ai_cords.0 as usize][ai_cords.1 as usize -1] = ai_name; }
    else if ai_direction == 4 { pa[ai_cords.0 as usize][ai_cords.1 as usize + 1] = ai_name; }
    drop(pa);
    let lost_piece = check_lost_piece(play_area.board.lock().unwrap());
    if *lost_piece.x.lock().unwrap() != -1{
        let mut play_area = &play_area.board;
        let mut pa = play_area.lock().unwrap();
        let x_ind = *lost_piece.x.lock().unwrap() as usize;
        let y_ind = *lost_piece.y.lock().unwrap() as usize;
        pa[x_ind][y_ind] =  0
    }
    *lost_piece.x.lock().unwrap();
    let win = check_win(play_area);
    return win;
}

#[tauri::command]
fn change_game(pa_state:State<'_,GameB>, game:String){
    let mut clean_game_state: GameB = GameB{
        board_size: Mutex::new(0),
        win_spaces: Mutex::new(vec![(0,0)]),
        board: Mutex::new(vec![vec![0; 8]; 8]),
        clean_board: Mutex::new(vec![]),
    };
    if game == "hen" { clean_game_state = game_rules::create_clean_hen_board(); }
    else if game == "ard" { clean_game_state = game_rules::create_clean_ard_board(); }
    else if game == "bra" { clean_game_state = game_rules::create_clean_bra_board(); }
    let mut play_area_board: MutexGuard<Vec<Vec<i8>>> = pa_state.board.lock().unwrap();
    let mut play_area_clean: MutexGuard<Vec<Vec<i8>>> = pa_state.clean_board.lock().unwrap();
    let mut play_area_size: MutexGuard<i8> = pa_state.board_size.lock().unwrap();
    let mut play_area_win: MutexGuard<Vec<(i8, i8)>> = pa_state.win_spaces.lock().unwrap();
    *play_area_board = clean_game_state.board.lock().unwrap().to_vec();
    *play_area_clean = clean_game_state.clean_board.lock().unwrap().to_vec();
    *play_area_size = *clean_game_state.board_size.lock().unwrap();
    *play_area_win  = clean_game_state.win_spaces.lock().unwrap().to_vec();
}

fn find_neighbors(pa: MutexGuard<Vec<Vec<i8>>>, x:i8, y:i8) -> Vec<(i8, i8, i8)>{
    let mut neighbors: Vec<(i8, i8, i8)> = vec![];
    let mut x_us = x as usize;
    let mut y_us = y as usize;
    neighbors.push((x, y, pa[x_us][y_us-1]));
    neighbors.push((x, y, pa[x_us][y_us+1]));
    neighbors.push((x, y, pa[x_us-1][y_us]));
    neighbors.push((x, y, pa[x_us+1][y_us]));
    return neighbors
}


fn check_lost_piece(pa: MutexGuard<Vec<Vec<i8>>>) -> ButtonObj{
    let size: usize =pa.len();
    for x in 1..size -1{
        for y in 1..size -1{
            if (pa[x][y] != 0) & (pa[x][y] != 3){
                let board_l = (pa[x][y-1] != pa[x][y]) & (pa[x][y-1] != 0);
                let board_r = (pa[x][y+1] != pa[x][y]) & (pa[x][y+1] != 0);
                let board_ch = (pa[x][y+1] == pa[x][y-1]);
                if (board_r && board_l) & board_ch{
                    return ButtonObj{
                        x:Mutex::new(x as i8),
                        y:Mutex::new(y as i8),
                        name: Mutex::new(-1)
                    }
                }
                let board_u = (pa[x+1][y] != pa[x][y]) & (pa[x+1][y] != 0);
                let board_d = (pa[x-1][y] != pa[x][y]) & (pa[x-1][y] != 0);
                let board_cv = (pa[x-1][y] == pa[x+1][y]);
                if (board_u && board_d) && board_cv{
                    return ButtonObj{
                        x:Mutex::new(x as i8),
                        y:Mutex::new(y as i8),
                        name: Mutex::new(-1)
                    }
                }
            }else if  {(pa[x][y] == 3)} {
                let board_l = (pa[x][y-1] != 2) & (pa[x][y-1] != 0);
                let board_r = (pa[x][y+1] != 2) & (pa[x][y+1] != 0);
                let board_ch = (pa[x][y+1] == pa[x][y-1]);
                let board_cv = (pa[x-1][y] == pa[x+1][y]);
                let board_u = (pa[x+1][y] != 2) & (pa[x+1][y] != 0);
                let board_d = (pa[x-1][y] != 2) & (pa[x-1][y] != 0);
                let lg1 = (board_r && board_l) & board_ch;
                let lg2 = (board_u && board_d) && board_cv;
                if lg1 & lg2{
                    return ButtonObj{
                        x:Mutex::new(x as i8),
                        y:Mutex::new(y as i8),
                        name: Mutex::new(-1)
                    }
                }
            }
        }
    }
    return ButtonObj{
        x:Mutex::new(-1),
        y:Mutex::new(-1),
        name: Mutex::new(-1)
    }
}

fn check_win(pa: State<GameB>) -> i8{
    pa.board.try_lock().expect("Something went wrong :(");
    let win_spaces = pa.win_spaces.lock().unwrap();
    let board = pa.board.lock().unwrap();
    let win_space_size: usize = win_spaces.len();
    let board_size = *pa.board_size.lock().unwrap() as usize;
    for cords in 0..win_space_size {
        let x = win_spaces[cords].0 as usize;
        let y = win_spaces[cords].1 as usize;
        if board[x][y] == 3{
            return 2;
        }}
    for x in 0..board_size {
            for y in 0..board_size {
                if board[x][y] == 3{ return 0; }
            }
    }
    return 1;
}

fn main() {
    let board = game_rules::create_clean_hen_board();
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![register_play_click,get_space, change_game, get_board_size, ai_setup, ai_integration])
        .manage(GameB{ board_size: board.board_size, win_spaces: board.win_spaces, board: board.board, clean_board: board.clean_board })
        .manage(LastUserClick{
            last_click_white: ButtonObj { x: Mutex::new(0), y: Mutex::new(0), name: Mutex::new(0), },
            last_click_black: ButtonObj { x: Mutex::new(0), y: Mutex::new(0), name: Mutex::new(1), } })
        .manage(TurnManager{ white_turn: Mutex::new(true), ai: Mutex::new(0), ai_white: Mutex::new(false) })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
