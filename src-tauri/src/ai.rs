use tauri::State;
use crate::game_rules::GameB;

pub(crate) struct GameState {
    pub(crate) board: Vec<Vec<i8>>,
    pub(crate) distance_to_win: i8,
    pub(crate) cords_of_enemy: Vec<(i8, i8)>,
    pub(crate) next_move: ((i8, i8), i8)
}

impl Default for GameState {
    fn default() -> Self {
        GameState{
            board: vec![vec![0; 20]; 20],
            distance_to_win: 0,
            cords_of_enemy: vec![],
            next_move: ((0, 0), 0),
        }
    }
}

impl GameState {
    pub(crate) fn min_max_new_child(board: Vec<Vec<i8>>, x:i8, y: i8, direction: i8) -> GameState{
        GameState {
            board,
            distance_to_win: -1,
            cords_of_enemy: vec![],
            next_move: ((x, y), direction)
        }

    }
}

use crate::ai_minmax::min_max::search;

pub(crate) fn ai(board:Vec<Vec<i8>>, play_area: &State<GameB>, white_turn: bool) -> (((i8, i8), i8), i32) {
    let depth: i8 = 7; // Change to increase of decrease the depth. Warning anything over 7 will use significant resources
    let mut game_state:GameState = GameState::default();
    game_state.board = board;

    // for min max keep this uncommented
    search(&game_state, &play_area, depth, !white_turn, i32::MIN, i32::MAX, vec![] )
}

