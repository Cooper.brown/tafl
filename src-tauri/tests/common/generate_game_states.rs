use crate::GameState;
use crate::Game_rules;

pub(crate) fn generate_test_game_state() -> GameState {
    let mut game_state = GameState::default();
    game_rules = Game_rules::create_clean_hen_board();
    game_state.board = game_rules.board;
    return game_state;
}

