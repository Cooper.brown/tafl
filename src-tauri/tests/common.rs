

pub fn generate_test_game_state() -> GameState {
    let mut game_state = GameState::default();
    game_rules = hen::game_rules::create_clean_hen_board();
    game_state.board = game_rules.board;
    return game_state;
}
